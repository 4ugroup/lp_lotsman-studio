$('.creative').slick({
    arrows: true,
    autoplay: true,
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow: '<span class="creative-nav prev"></span>',
    nextArrow: '<span class="creative-nav next"></span>'
});


$('.model').slick({
    arrows: true,
    autoplay: true,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="model-nav prev"></span>',
    nextArrow: '<span class="model-nav next"></span>'
});

$('.contents').slick({
    arrows: false,
    autoplay: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1
});


$('.review').slick({
    arrows: true,
    autoplay: true,
    dots: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '<span class="review-nav prev"></span>',
    nextArrow: '<span class="review-nav next"></span>'
});



$(window).load(function() {

    $('.b01').removeClass('page-start');

    $('#fullpage').fullpage({
        scrollOverflow: true
    });
});


$(".btn-video").fancybox({
    'padding'    : 0,

    'tpl'        : {
        closeBtn : '<a title="Close" class="btn_close" href="javascript:;"></a>'
    }
});


$(function() {
    var pull = $('.btn-modal');
    var box = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        box.toggleClass('open');
    });

    $('.btn-close').click(function(){
        box.toggleClass('open');
    });
});

/* Portfolio */

$('.btn-portfolio').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $('.tabs');

    box.find('.tabs-nav-item').removeClass('active');
    box.find('.tabs-item').removeClass('active');

    console.log(tab);

    box.find(tab).addClass('active');

    $('.seven').addClass('full');
});


$('.btn-tab-nav').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');


    box.find('.tabs-nav-item').removeClass('active');
    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


$('.tabs-nav').slick({
    arrows: true,
    autoplay: false,
    dots: false,
    vertical: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    prevArrow: '<span class="tabs-nav-nav prev"></span>',
    nextArrow: '<span class="tabs-nav-nav next"></span>'
});

$('.your-element').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    console.log(nextSlide);
});


$('.tab1 .pf-slider').slick({
    dots: true,
    arrows: false,
    autoplay: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.tab2 .pf-slider').slick({
    dots: true,
    arrows: false,
    autoplay: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
});


$('.tab3 .pf-slider').slick({
    dots: true,
    arrows: false,
    autoplay: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.tab4 .pf-slider').slick({
    dots: true,
    arrows: false,
    autoplay: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.tab5 .pf-slider').slick({
    dots: true,
    arrows: false,
    autoplay: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.tab6 .pf-slider').slick({
    dots: true,
    arrows: false,
    autoplay: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.tab7 .pf-slider').slick({
    dots: true,
    arrows: false,
    autoplay: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

